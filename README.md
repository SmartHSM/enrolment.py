# Demonstration of Enigma Bridge Cloud Encryption Service

This service provides high-security encryption with keys protected by secure 
hardware (if you're into this, this includes FIPS140-2 Level 3 or Common Criteria
EAL5+).

The architecture has some basic differences from traditional use of encryption.
They have a significant impact on how you can use the service.

1. We have almost completely hidden away the concept of "key" and instead use
"object", or "user object" (UO). UO is a data structure, which contains all 
necessary keys, parameters for the cloud service SLA (different objects guarantee 
different throughput), and in some cases a script describing a sequence of 
basic operations that are executed with each use of the UO.
2. The operational security is provided via communication keys (ENC and MAC), 
which protect the data between your application and the service and also authorize
each request.
3. We support fully automated user enrolment, which has been designed for cloud
applications and full automation. More about the enrolment process is further down.
The user enrolment service is completely separate from the actual encryption 
service and allows centralized management of users, as well as "service in a box"
deployments.

## Examples

This project contains 2 examples. 
- **use_only_example.py** - this demonstration shows the simplicity of the use once an 
appropriate object (UO) has been created.
- **complete_example.py** - this demonstration shows the whole life-cycle, from 
enrolment of new user to the first encryption.

## Enrolment Process

Enrolment process in the enclosed example runs against hut3.enigmabridge.com .
It consists of the following stages.

1. `InitClientAuthRequest` - a simple, optional, query, which provides the client
with information about any relevant pre-authorization requirements.
2. `RegistrationRequest` - this function requests creation of a new user. It uses
an authorization token. This token must be already registered on the registration 
server. It also determines what operations new users can perform and how many
transactions per second they can run.
3. `ApiKeyRequest` - with a username and password for a new user, we can request
our first API key. The response provides all necessary information to connect to
the correct encryption servers - `Configuration`.

## Encryption Management

Once the enrolment is complete and the client has its API key, it can start
creating objects (UOs), i.e., encryption keys.

1. `new_uo = CreateUO(configuration=Configuration)`
2. `uo_aes = new_uo.create_uo(obj_type=UOTypes.PLAINAES)`

The `uo_aes` will provide the following information:
 - object handle (uo_id)
 - communication keys (enc_key, mac_key)
 
## Doing Encryption

The last step is the actual operation - data encryption. 

1. `pd = ProcessData(uo=uo_aes, config=Configuration)`
2. `data_encrypted = pd.call(from_hex(data_plain))`

## Supported Operations

Here is a list of operations, which the service supports:

1. **User Authentication** - passwords and one-time passwords
  - HMAC - fingerprint from the data
  - SCRAMBLE - password processing for unhackable password databases
  - ENSCRAMBLE - like SCRAMBLE, but the communication has an additional encryption
  - AUTH_HOTP - user authentication with one-time passwords (OCRA HOTP)
  - AUTH_NEW_USER_CTX - create a user context with counters for failures and a policy
  - AUTH_PASSWORD - user authentication with passwords
  - AUTH_UPDATE_USER_CTX - update authentication context for a user
2. **Encryption**
  - PLAINAES - usual data encryption
  - PLAINAESDECRYPT - data decription
  - RSA1024DECRYPT_NOPAD - data signing with a short key
  - RSA2048DECRYPT_NOPAD - data signing with a strong key
  - RSA1024ENCRYPT_NOPAD - encryption with public key - short key
  - RSA2048ENCRYPT_NOPAD - encryption with public key - strong key
  - EC_FP192SIGN - data signing with an alternative algorithm (ECC)
3. **Tokenization**
  - GENTOKENIZETABLEROW - create tokenization table - PCI DSS compliant
  - TOKENIZE - tokenization, PCI DSS compliant
  - DETOKENIZE - detokenization
4. **Secure data transport**
  - AES2RSA - re-encryption from database to communication key
  - RSA2AES - re-encryption from communication key to database key
  - DES2RSA - database key re-encryption - from legacy algorithm to new one
  - RSA2DES - database key re-encryption - from new one to legacy algorithm
  - TOKENIZEWRAP - tokenization with encryption to send the data to third-party
5. **Financial functions - DUKPT**
  - DUKPT_REWRAP - financial transactions - DUKPT rewrap
  - DUKPT_NEWPAYTERMINAL - financial transactions - DUKPT register a new terminal
  - DUKPT_SESSIONKEYS - financial transactions - DUKPT session keys
  - DUKPT_PINTRANSLATE - financial transactions - DUKPT PIN translation
  - DUKPT - financial transactions - DUKPT encryption
6. **Secure key distribution**
  - GENWRAP_GENERATEKEY - key generation for secure distribution
  - GENWRAP_UNWRAPKEY - unwrapping key for secure deployment
7. **Random number generation, metering**
  - RANDOMDATA - generate random data with a FIPS140-2 compliant generator (RNG)
  - RSA_GENERATE - generate RSA key
  - METERING - NOP/empty operation for measuring number of operations

