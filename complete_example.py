#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
***
Module: demonstration of the full enrolment process to Enigma Bridge SmartHSM. Please read the attached README file
        for further explanations.
***

 Copyright (C) Enigma Bridge Ltd, registered in the United Kingdom.
 This file is owned exclusively by Enigma Bridge Ltd.
 Unauthorized copying of this file, via any medium is strictly prohibited
 The code is provided under the MIT license

 Written by Enigma Bridge Ltd <support@enigmabridge.com>, August 2018
"""
from datetime import datetime
# import random
# import string
# import json
import pickle

from ebclient.create_uo import TemplateFields, Environment, Gen
from ebclient.eb_consts import UOTypes
from ebclient.eb_create_uo import CreateUO, TemplateKey
from ebclient.process_data import ProcessData

__author__ = "Enigma Bridge"
__copyright__ = 'Enigma Bridge Ltd'
__email__ = 'support@enigmabridge.com'
__status__ = 'Development'


from ebclient.eb_configuration import *
from ebclient.eb_registration import *
from ebclient.registration import *

AUTH_TOKEN = 'replace with token'

cfg = Configuration()

cfg.endpoint_register = Endpoint.url('https://hut3.enigmabridge.com:8445')

#######################################################################################
# #
# #  We start we querying a user management server. For LUCY, where each client has a
# #  standalone on-site servers, this user management will be on the same server as SmartHSM/TLM
# #
#######################################################################################

# # Step 0: an optional query, which shows whether the client needs to obtain an authorization data
# client_data_req = {
#             'type': 'lucy_db_master_key',  # this identifies a particular type - in this case LUCY...
#             'method': 'type'
#         }
#
# # registration based on method "type" returns an empty list of requirements
# regreq = InitClientAuthRequest(client_data=client_data_req, env=ENVIRONMENT_PRODUCTION, config=cfg)
# regresponse = regreq.call()

#
#
# Step 1: we use the enrolment credentials - luch_db_master_key, and request a client authorization data
#######################################################################################
client_data_reg = {
    'name': "lucy installation",     # max 100 characters - should be unique but it's only for visualization in UI
    'authentication': 'type',
    'type': 'lucy_db_master_key',   # value generated as part of the SmartHSM delivery for on-site deployment
    'token': AUTH_TOKEN             # value generated as part of the SmartHSM delivery for on-site deployment
}

regreq = RegistrationRequest(client_data=client_data_reg, env=ENVIRONMENT_PRODUCTION, config=cfg)
client_record = regreq.call()
# an example of the value returned for the new client - serialized with json.dumps(client_record)
# client_record = '{"username": "username", "status": "enabled", "authenticationType": "password", "max_api_keys": 5,
# "maxobjects": 10, "password": "password"}'

print("client username: %s" % client_record['username'])
print("client password: %s" % client_record['password'])
print("authentication type: %s" % client_record['authenticationType'])
print("SLA params: max API keys: %d, max objects: %d" % (client_record['max_api_keys'], client_record['maxobjects']))

# Step 2: request an API key - this allows to the SmartHSM API
#######################################################################################
client_api_req = {
    'authentication': client_record['authenticationType'],  # this is "password" for the provided enrolment token
    'username': client_record['username'],
    'password': client_record['password']
}

endpoint = {  # all items in this structure are optional - the "endpoint" can be an empty object
    "ipv4": "123.23.23.23",
    "ipv6": "fe80::2e0:4cff:fe68:bcc2/64",
    "country": "gb",
    "network": "fastnetwork",
    "location": [0.34, 10]
}

apireq = ApiKeyRequest(client_data=client_api_req, endpoint=endpoint, env=ENVIRONMENT_PRODUCTION, config=cfg)
apiresponse = apireq.call()
if 'apikey' not in apiresponse:  # a debug check
    raise Exception('ApiKey was not present in the getApiKey response')

# an example of the response - serialized with json.dumps(apiresponse)
# {"status": "enabled", "operations": [{"status": "enabled", "operation": 15, "operationname": "basic_aes128_decrypt"},
#  {"status": "enabled", "operation": 4, "operationname": "basic_aes128_encrypt"}, {"status": "enabled", "operation": 5,
# "operationname": "basic_rsa1024_decrypt"}, {"status": "enabled", "operation": 6, "operationname":
# "basic_rsa2048_decrypt"}], "apikey": "xma10gsdyszeskkghv6s7twtkwgvi5m276n0nqoy9w8mbyxijdodjyqf2tvcdq8p",
# "servers": [{"domain": "girton", "name": "gb_sovhouse", "useEndpoints": [{"protocol": "tcp", "port": 11110},
# {"protocol": "https", "port": 11180}], "fqdn": "gb-1.enigmabridge.com", "environment": "prod", "enrolEndpoints":
#  [{"protocol": "https", "port": 11182}, {"protocol": "tcp", "port": 11112}]}]}

print("status: %s" % apiresponse['status'])
print("apikey: %s" % apiresponse['apikey'])
print("List of operations")
for one_op in apiresponse['operations']:
    print("    %d (%s) - %s" % (one_op['operation'], one_op['operationname'], one_op['status']))

print("List of HSM servers")
for one_server in apiresponse['servers']:
    print("    name: %s, trust domain: %s, env: %s, url: %s" %
          (one_server['name'], one_server['domain'], one_server['environment'], one_server['fqdn']))
    print("      use endpoints:")
    for one_use_ep in one_server['useEndpoints']:
        print("          protocol: %s, port: %d" % (one_use_ep['protocol'], one_use_ep['port']))
    print("      enrol endpoints")
    for one_use_ep in one_server['enrolEndpoints']:
        print("          protocol: %s, port: %d" % (one_use_ep['protocol'], one_use_ep['port']))


# we can now update the configuration with the SmartHSM servers we want to use
if apiresponse['servers'][0]['useEndpoints'][0]['protocol'] == 'https':
    ep_1 = apiresponse['servers'][0]['useEndpoints'][0]['protocol'] + "://" \
           + apiresponse['servers'][0]['fqdn'] + ":" + str(apiresponse['servers'][0]['useEndpoints'][0]['port'])
else:
    ep_1 = apiresponse['servers'][0]['useEndpoints'][1]['protocol'] + "://" \
           + apiresponse['servers'][0]['fqdn'] + ":" + str(apiresponse['servers'][0]['useEndpoints'][1]['port'])

if apiresponse['servers'][0]['enrolEndpoints'][0]['protocol'] == 'https':
    ep_2 = apiresponse['servers'][0]['enrolEndpoints'][0]['protocol'] + "://" \
           + apiresponse['servers'][0]['fqdn'] + ":" + str(apiresponse['servers'][0]['enrolEndpoints'][0]['port'])
else:
    ep_2 = apiresponse['servers'][0]['enrolEndpoints'][1]['protocol'] + "://" \
           + apiresponse['servers'][0]['fqdn'] + ":" + str(apiresponse['servers'][0]['enrolEndpoints'][1]['port'])

cfg.endpoint_process = Endpoint.url(ep_1)
cfg.endpoint_enroll = Endpoint.url(ep_2)
cfg.api_key = apiresponse['apikey']
if apiresponse['servers'][0]['environment'] == 'prod':
    cfg.environment = Environment.PROD
elif apiresponse['servers'][0]['environment'] == 'dev':
    cfg.environment = Environment.DEV
elif apiresponse['servers'][0]['environment'] == 'test':
    cfg.environment = Environment.TEST


#######################################################################################
# #
# #  We now connect to the actual security module, where we firstly create a new UO
# #  which gives us access to the encryption/decryption
# #
#######################################################################################

cfg = pickle.loads(b'\x80\x03cebclient.eb_configuration\nConfiguration\nq\x00)\x81q\x01}q\x02(X\x10\x00\x00\x00endpoint_processq\x03cebclient.eb_configuration\nEndpoint\nq\x04)\x81q\x05}q\x06(X\x06\x00\x00\x00schemeq\x07X\x05\x00\x00\x00httpsq\x08X\x04\x00\x00\x00hostq\tX\x15\x00\x00\x00gb-1.enigmabridge.comq\nX\x04\x00\x00\x00portq\x0bM\xac+ubX\x0f\x00\x00\x00endpoint_enrollq\x0ch\x04)\x81q\r}q\x0e(h\x07X\x05\x00\x00\x00httpsq\x0fh\tX\x15\x00\x00\x00gb-1.enigmabridge.comq\x10h\x0bM\xae+ubX\x11\x00\x00\x00endpoint_registerq\x11h\x04)\x81q\x12}q\x13(h\x07X\x05\x00\x00\x00httpsq\x14h\tX\x15\x00\x00\x00hut3.enigmabridge.comq\x15h\x0bM\xfd ubX\x07\x00\x00\x00api_keyq\x16X@\x00\x00\x000i17n6u93evhuvnqiycm3v5g4ttskbiiax4nqr03liopre5r52rh7s4hly5o0l1vq\x17X\x0b\x00\x00\x00http_methodq\x18X\x04\x00\x00\x00POSTq\x19X\x06\x00\x00\x00methodq\x1aX\x04\x00\x00\x00RESTq\x1bX\x07\x00\x00\x00timeoutq\x1cJ\x90_\x01\x00X\x05\x00\x00\x00retryq\x1dcebclient.eb_configuration\nSimpleRetry\nq\x1e)\x81q\x1f}q (X\t\x00\x00\x00max_retryq!K\x03X\x0b\x00\x00\x00jitter_baseq"K\xc8X\x0b\x00\x00\x00jitter_randq#K2ubX\n\x00\x00\x00create_tplq$}q%X\x0b\x00\x00\x00environmentq&X\x04\x00\x00\x00prodq\'ub.')

# Step 3: request a new object = (i.e., encryption key)
#######################################################################################
new_uo = CreateUO(configuration=cfg)
new_uo_inv = CreateUO(configuration=cfg)

tpl = {  # this definition will update the initial configuration
    TemplateFields.environment: Environment.PROD,
    TemplateFields.FLAG_APP_GEN: Gen.CLIENT  # a flag signalling an application key source is coming from the client
                                             # this is not the actual key - it will be changed with SmartHSM master key
}

# we provide an initial application key value from the client to share it between 2 objects AES ENCRYPT and DECRYPT
client_keys = {
    'app': TemplateKey(key=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00', key_type='app')
}

uo_aes = new_uo.create_uo(tpl=tpl, obj_type=UOTypes.PLAINAES, keys=client_keys)
uo_aes_inv = new_uo_inv.create_uo(tpl=tpl, obj_type=UOTypes.PLAINAESDECRYPT, keys=client_keys)

print("We have created an object")
print("  ID/handle: %d (object type: %d)" % (uo_aes.uo_id, uo_aes.uo_type))
print("  communication enc key %s" % base64.b16encode(uo_aes.enc_key).decode('ascii'))
print("  communication MAC key %s" % base64.b16encode(uo_aes.mac_key).decode('ascii'))

#
#
# Step 4: encrypt data ... and decrypt data ... and compare
#######################################################################################

pd = ProcessData(uo=uo_aes, config=cfg)
pd_inv = ProcessData(uo=uo_aes_inv, config=cfg)

data_plain = '6bc1bee22e409f96e93d7e117393172a'
result = pd.call(from_hex(data_plain))
x1 = base64.b16encode(result).decode('ascii')

result_inv = pd_inv.call(from_hex(x1))

data_result = base64.b16encode(result_inv).decode('ascii')

if data_result.lower() == data_plain.lower():
    print("OK - decrypted data is the same as the initial data: %s" % data_plain)
else:
    print("ERROR - decrypted data differs from the initial data: %s -> %s" % (data_plain, data_result))
